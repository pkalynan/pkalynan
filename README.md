- 👋 Hi, I’m @pkalynan
- 👀 I’m interested in ...
- 🌱 I’m currently learning ...
- 💞️ I’m looking to collaborate on ...
- 📫 How to reach me ...

<!---
pkalynan/pkalynan is a ✨ special ✨ repository because its `README.md` (this file) appears on your GitHub profile.
You can click the Preview link to take a look at your changes.
--->

Hi, I'm @pkalynan. I'm interested in software and statistics. I'm currently learning languages usually on Ubuntu particularly C++ and C and adapting to new concepts. I'm looking to collaborate on technical articles, how-to guides and other tutorial oriented content based on technology.
Reach me by messaging through GitHub.

Studied [Data Analytics](https://www.snhu.edu/online-degrees/masters/ms-in-data-analytics) at a graduate level and data science at a certificate level. Mixtures of IT, Analytics and Statistics would sum up what I have studied. While my first degree was in [Business](https://business.humber.ca/future-students/explore/full-time/credential/degrees.html), I have always been around technology.

Currently, I have been working on technical writing and applying knowledge/skills into my articles. I assist with web site design and domain name system information for a [close contact of mine](http://pavithraconsulting.ca/). I have published a few of my articles on Analytics Vidhya through Medium.com as well as particpated in ["Data Science Blogathon 5"](https://datahack.analyticsvidhya.com/contest/data-science-blogathon-5/) with articles about [Visualizing PCA in R-Programming](https://www.analyticsvidhya.com/blog/2021/02/visualizing-pca-in-r-programming-with-factoshiny/) and [the Importance of Cleaning and Cleansing](https://www.analyticsvidhya.com/blog/2021/02/the-importance-of-cleaning-and-cleansing-your-data/). Published on Analytics Vidhya via Medium.com with articles about [Python Tkinter Applications as Java Applications](https://medium.com/analytics-vidhya/python-tkinter-as-a-java-application-36536176fe83) and [Trying to Avoid Digital Scams](https://medium.com/analytics-vidhya/trying-to-avoid-digital-scams-7d4f1e665119). Recently, Section.io's Engineering Education platform published my tutorial on ["Verifying Website Domains"](https://www.section.io/engineering-education/verifying-website-domains/).

Sample topics include: Internet domain name system records, VirtualBox, Operating Systems, programming/coding for data sciences and other helpful applications.
Few pending topics currently in the works are: Kali Linux on Windows and basic statistics.
